def buildJar() {
    echo "---> Building the application..."
    sh "mvn package"
}

def buildImage() {
    echo "---> Building the docker image..."
    withCredentials([
        usernamePassword(credentialsId: "docker-hub-joseph7576", usernameVariable: "DOCKER_USER", passwordVariable: "DOCKER_PASS") 
    ]) {
        sh "docker build -t joeho7576/playground-demo-java:jma-2.0 ."

        echo "---> Pushing docker image to repository..."
        sh "echo $DOCKER_PASS | docker login -u $DOCKER_USER --password-stdin"
        sh "docker push joeho7576/playground-demo-java:jma-2.0"
    }
}

def deployApp() {
    echo "---> Deploying application..."
}

return this
